const expressJwt = require('express-jwt');
const config = require('config.json');
const userService = require('../users/user.service');

module.exports = jwt;

function jwt() {
    const secret = config.secret;
    console.log('inside jwt');
    return expressJwt({
        secret,
        isRevoked,
        getToken: function getTokenFromCookie(req) {
            console.log('req.cookies', req.cookies);
            if (req.cookies && req.cookies.jwt) {
                return req.cookies.jwt;
            }
            return null;
        }
    }).unless({
        path: [
            // public routes that don't require authentication
            '/users/authenticate',
            '/users/register'
        ]
    });
}

async function isRevoked(req, payload, done) {
    const user = await userService.getById(payload.sub);
    // console.log('done', done);
    // console.log('payload', payload);
    // console.log('isRevoked', user);
    // revoke token if user no longer exists
    if (!user) {
        return done(null, true);
    }

    done();
};
