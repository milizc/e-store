const cors = require('cors');

module.exports = corsMiddleware;

function corsMiddleware() {
    const options = {
        credentials: true,
        origin: 'http://localhost.labs.com:8080',
        methods: ['GET', 'PUT', 'POST', 'DELETE', 'UPDATE', 'OPTIONS']
    };
    return cors(options);
}