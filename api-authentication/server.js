﻿require('rootpath')();
const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const uuid = require('uuid/v4');
const session = require('express-session');

const app = express();
const config = require('config.json');
var FileStore = require('session-file-store')(session);
const corsMiddleware = require('_helpers/cors-middeware');
const jwt = require('_helpers/jwt');
const errorHandler = require('_helpers/error-handler');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(corsMiddleware());
app.use(cookieParser());

app.use(session({
    genid: (req) => {
        return uuid();
    },
    name: 'sid',
    secret: config.secretSession,
    resave: false,
    saveUninitialized: true,
    store: new FileStore()
}));

app.use(function printSession(req, res, next) {
    console.log('req.sessionID', req.sessionID);
    console.log('', req.session);
    return next();
});

// use JWT auth to secure the api
app.use(jwt());

// api routes
app.use('/users', require('./users/users.controller'));

// app.route('/users').get(jwt());
// global error handler
app.use(errorHandler);

// start server
const port = process.env.NODE_ENV === 'production' ? 80 : 4000;

const server = app.listen(port, function () {
    console.log('Server listening on port ' + port);
});
