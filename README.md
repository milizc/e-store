## PoC node - JWT - cookies

Este proyecto incluye PoCs con:
- [Node](https://nodejs.org/es/)
- [JWT](https://jwt.io/)
- Cookies HttpOnly
- [MongoDB](https://docs.mongodb.com/manual/)
